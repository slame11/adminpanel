<?php

namespace App\Http\Controllers;

use App\WokenUpModels\StatusesReport;
use App\User;
use Illuminate\Http\Request;
use App\WokenUpModels\Country;
use App\WokenUpModels\Gender;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = new User();


        if ($request->created_from) {
            $users = $users->where('created_at','>=', $request->created_from);
        }

        if ($request->created_to) {
            $users = $users->where('created_at','<=', $request->created_to);
        }

        if ($request->order && $request->sort) {
            $users = $request->order == 'user'
                ? $users->orderBy('firstname', $request->sort)->orderBy('lastname', $request->sort)
                : $users->orderBy($request->order, $request->sort);
        } else {
            $users = $users->orderBy('id', 'DESC');
        }

        return view('admin.index', [
            'title' => __('general.users'),
            'users' => $users->paginate(),
            'request' => collect($request->all()),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create', [
            'countries' => Country::orderBy('name')->get()->pluck('name', 'id'),
            'genders' => Gender::orderBy('title')->get()->pluck('title', 'id'),
            'user' => new User,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|min:8|confirmed',
        ]);

        $arr = $request->all();
        $arr['password'] = Hash::make($arr['password']);
        User::create($arr);

        return redirect()
            ->route('admins.index')
            ->with('message.level', 'success')
            ->with('message.content', 'User successfully added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.edit', [
            'user' => User::where('id', $id)->firstOrFail(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user)
    {

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'password' => 'confirmed',
        ]);
        $user = User::find($user)->first();
        $user->fill($request->except('password'));

        if ($request['password'] != '') {
            $user->password = Hash::make($request['password']);
        }

        $user->save();

        return redirect()
            ->route('admins.index')
            ->with('message.level', 'success')
            ->with('message.content', 'User successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($user)
    {

        User::find($user)->delete();

        return redirect()
            ->route('admins.index')
            ->with('message.level', 'success')
            ->with('message.content', 'User successfully deleted.');
    }
}
