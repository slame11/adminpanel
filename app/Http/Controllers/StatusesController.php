<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 13.11.2019
 * Time: 21:39
 */

namespace App\Http\Controllers;


use App\WokenUpModels\Organization;
use App\WokenUpModels\Status;
use App\WokenUpModels\User;
use App\WokenUpModels\ViewType;
use Illuminate\Http\Request;

class StatusesController
{
    public function index(Request $request){

        $posts = Status::select('statuses.*')
            ->leftJoin('users','statuses.user_id','=','users.id')
            ->leftJoin('organizations','statuses.organization_id','=','organizations.id')
            ->with('user')->with('organization');

        if ($request->user) {
            $posts = $posts->where('statuses.user_id', $request->user);
        }

        if ($request->organisation) {
            $posts = $posts->where('statuses.organization_id', $request->organisation);
        }

        if ($request->created_from) {
            $posts = $posts->where('statuses.created_at','>=', $request->created_from);
        }

        if ($request->created_to) {
            $posts = $posts->where('statuses.created_at','<=', $request->created_to);
        }

        if ($request->order && $request->sort) {
            $posts = $request->order == 'statuses.created_at'
                ? $posts->orderBy('statuses.created_at', $request->sort)
                : $posts->orderBy($request->order, $request->sort);
        }

        return view('posts.index',[
            'title' => __('general.posts'),
            'posts' => $posts->paginate(15),
            'request' => collect($request->all()),
            'users' => User::all(),
            'organisations' => Organization::all(),

        ]);
    }

    public function create()
    {
        return view('posts.create', [
            'status' => new Status,
            'organizations' => Organization::orderBy('title')->get()->pluck('title', 'id'),
            'parents' => Status::orderBy('body')->pluck('body', 'id'),
            'view_types' => ViewType::orderBy('title')->get()->pluck('title', 'id'),
            'users' => User::orderBy('username')->get()->pluck('username', 'id')
        ]);
    }

    public function edit(Status $status)
    {
        return view('posts.edit', [
            'status' => $status,
            'organizations' => Organization::orderBy('title')->get()->pluck('title', 'id'),
            'parents' => Status::where('id', '!=', $status->id)->orderBy('body')->pluck('body', 'id'),
            'view_types' => ViewType::orderBy('title')->get()->pluck('title', 'id'),
            'users' => User::orderBy('username')->get()->pluck('username', 'id')
        ]);
    }

    public function store(Request $request)
    {
        Status::create($request->all());

        return redirect()
            ->route('statuses.index')
            ->with('message.level', 'success')
            ->with('message.content', 'Post successfully added.');
    }

    public function update(Request $request, Status $status)
    {
        $status->fill($request->all())->save();

        return redirect()
            ->route('statuses.index')
            ->with('message.level', 'success')
            ->with('message.content', 'Post successfully updated.');
    }

    public function destroy(Status $status)
    {
        $status->delete();

        return redirect()
            ->route('statuses.index')
            ->with('message.level', 'success')
            ->with('message.content', 'Post successfully deleted.');
    }
}