<?php

namespace App\Http\Controllers;

use App\WokenUpModels\Country;
use App\WokenUpModels\IndustrySubtype;
use App\WokenUpModels\IndustryType;
use App\WokenUpModels\Organization;
use App\WokenUpModels\OrganizationSize;
use App\WokenUpModels\OrganizationType;
use App\WokenUpModels\User;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $organizations = new Organization;
        if ($request->order && $request->sort) {
            switch ($request->order) {
                case 'user':
                    $organizations = $organizations
                        ->join('users', 'users.id', '=', 'organizations.user_id')
                        ->orderBy('users.firstname', $request->sort)
                        ->orderBy('users.lastname', $request->sort);
                    break;
                case 'type';
                    $organizations = $organizations
                        ->join('organization_types', 'organization_types.id', '=', 'organizations.organization_type_id')
                        ->orderBy('organization_types.title', $request->sort);
                    break;
                default:
                    $organizations = $organizations->orderBy($request->order, $request->sort);
                    break;
            }
        } else {
            $organizations = $organizations->orderBy('id', 'DESC');
        }

        return view('organization.index', [
            'title' => __('general.organizations'),
            'organizations' => $organizations->paginate(),
            'request' => collect($request->all()),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('organization.create', [
            'users' => User::orderBy('firstname')->get()->pluck('fullname', 'id'),
            'countries' => Country::orderBy('name')->get()->pluck('name', 'id'),
            'organization_types' => OrganizationType::orderBy('title')->get()->pluck('title', 'id'),
            'organization_sizes' => OrganizationSize::orderBy('title')->get()->pluck('title', 'id'),
            'industry_types' => IndustryType::orderBy('title')->get()->pluck('title', 'id'),
            'industry_subtypes' => IndustrySubtype::orderBy('title')->get()->pluck('title', 'id'),
            'organization' => new Organization,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->save($request);

        return redirect()
            ->route('organizations.index')
            ->with('message.level', 'success')
            ->with('message.content', 'Organization successfully added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('organization.edit', [
            'users' => User::orderBy('firstname')->get()->pluck('fullname', 'id'),
            'countries' => Country::orderBy('name')->get()->pluck('name', 'id'),
            'organization_types' => OrganizationType::orderBy('title')->get()->pluck('title', 'id'),
            'organization_sizes' => OrganizationSize::orderBy('title')->get()->pluck('title', 'id'),
            'industry_types' => IndustryType::orderBy('title')->get()->pluck('title', 'id'),
            'industry_subtypes' => IndustrySubtype::orderBy('title')->get()->pluck('title', 'id'),
            'organization' => Organization::findOrFail($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->save($request, $id);

        return redirect()
            ->route('organizations.index')
            ->with('message.level', 'success')
            ->with('message.content', 'Organization successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $organization = Organization::findOrFail($id);
        $organization->delete();

        return redirect()
            ->route('organizations.index')
            ->with('message.level', 'success')
            ->with('message.content', 'Organization successfully deleted.');
    }

    protected function save(Request $request, $id = null)
    {
        $organization = Organization::findOrNew($id);
        $organization->fill($request->except('_token'));
        $organization->is_premium = $request->is_premium ?? 0;
        $organization->messaging = $request->messaging ?? 0;
        $organization->user_acceptance = $request->user_acceptance ?? 0;
        $organization->stripe_active = $request->stripe_active ?? 0;
        $organization->save();
    }
}
