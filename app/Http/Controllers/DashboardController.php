<?php

namespace App\Http\Controllers;

use App\WokenUpModels\IndustrySubtype;
use App\WokenUpModels\IndustryType;
use App\WokenUpModels\Gender;
use App\WokenUpModels\Organization;
use App\WokenUpModels\OrganizationType;
use App\WokenUpModels\Session;
use App\WokenUpModels\Status;
use App\WokenUpModels\TagTypes;
use App\WokenUpModels\UnsdgTypes;
use App\WokenUpModels\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    public $ages_variants = [[13,17],[18,24],[25,29],[30,34],[35,39],[40,49],[50,59],[60,200]];

    public function getOrgTypesToUnsdgTags($time_period = false, $undsg_tags = []){
        $organisation_types =  OrganizationType::all();
        $usndg_types = UnsdgTypes::all();

        if(in_array('Any', $undsg_tags)){
            $undsg_tags = false;
        }

        $organisation_types_to_unsdg_tags = [];
        foreach ($organisation_types as $organisation_type){
            $item = DB::connection('mysql_remote')->table('categorizations')
                ->join('statuses', 'statuses.id', '=', 'categorizations.model_id')
                ->join('organizations', 'organizations.id', '=', 'statuses.organization_id')
                ->where('organizations.organization_type_id',$organisation_type->id)
                ->where('categorizations.model_type','like', '%Status')
                ->where('categorizations.segment_type', 'like','%UnsdgType')
                ->whereIn('categorizations.segment_id',$undsg_tags ? $undsg_tags : $usndg_types->pluck('id')->all());


            $item = $this->filterPeriod($item, $time_period);


            $organisation_types_to_unsdg_tags[]  = $item->count();
        }
        //add individual user type
        $item = DB::connection('mysql_remote')->table('categorizations')
            ->join('statuses', 'statuses.id', '=', 'categorizations.model_id')
            ->where('statuses.organization_id',null)
            ->where('categorizations.model_type','like', '%Status')
            ->where('categorizations.segment_type', 'like','%UnsdgType')
            ->whereIn('categorizations.segment_id',$undsg_tags ? $undsg_tags : $usndg_types->pluck('id')->all());


        $item = $this->filterPeriod($item, $time_period);
        $organisation_types_to_unsdg_tags[] = $item->count();

        return $organisation_types_to_unsdg_tags;
    }

    public function getOrgTypesToTags($time_period = false, $tags = []){
        $organisation_types =  OrganizationType::all();
        $tags_types = TagTypes::all();

        if(in_array('Any', $tags)){
            $tags = false;
        }

        $organisation_types_to_tags = [];
        foreach ($organisation_types as $organisation_type){
               $item =  DB::connection('mysql_remote')->table('categorizations')
                ->join('statuses', 'statuses.id', '=', 'categorizations.model_id')
                ->join('organizations', 'organizations.id', '=', 'statuses.organization_id')
                ->where('organizations.organization_type_id',$organisation_type->id)
                ->where('categorizations.model_type','like', '%Status')
                ->where('categorizations.segment_type', 'like','%TagType')
                ->whereIn('categorizations.segment_id', $tags ? $tags : $tags_types->pluck('id')->all());

            $item = $this->filterPeriod($item, $time_period);

            $organisation_types_to_tags[] = $item->count();
        }

        //add individual user type
        $item = DB::connection('mysql_remote')->table('categorizations')
            ->join('statuses', 'statuses.id', '=', 'categorizations.model_id')
            ->where('statuses.organization_id',null)
            ->where('categorizations.model_type','like', '%Status')
            ->where('categorizations.segment_type', 'like','%TagType')
            ->whereIn('categorizations.segment_id',$tags ? $tags : $tags_types->pluck('id')->all());


        $item = $this->filterPeriod($item, $time_period);
        $organisation_types_to_tags[] = $item->count();

        return $organisation_types_to_tags;
    }

    public function getGenderToUnsdgTags($time_period = false, $undsg_tags = []){
        $genderTypes = Gender::all();
        $usndg_types = UnsdgTypes::all();

        if(in_array('Any', $undsg_tags)){
            $undsg_tags = false;
        }

        $gender_to_undsgtags = [];
        foreach($genderTypes as $genderType){
            $item =  DB::connection('mysql_remote')->table('categorizations')
                ->join('statuses', 'statuses.id', '=', 'categorizations.model_id')
                ->join('organizations', 'organizations.id', '=', 'statuses.organization_id')
                ->join('users', 'organizations.user_id', '=', 'users.id')
                ->where('users.gender_id',$genderType->id)
                ->where('categorizations.model_type','like', '%Status')
                ->where('categorizations.segment_type', 'like','%UnsdgType')
                ->whereIn('categorizations.segment_id', $undsg_tags ? $undsg_tags : $usndg_types->pluck('id')->all());

            $item = $this->filterPeriod($item, $time_period);

            $gender_to_undsgtags[] = $item->count();
        }

        return $gender_to_undsgtags;
    }

    public function getGenderToTags($time_period = false, $tags = []){
        $genderTypes = Gender::all();
        $tags_types = TagTypes::all();

        if(in_array('Any', $tags)){
            $tags = false;
        }

        $gender_to_tags = [];
        foreach($genderTypes as $genderType){
            $item =  DB::connection('mysql_remote')->table('categorizations')
                ->join('statuses', 'statuses.id', '=', 'categorizations.model_id')
                ->join('organizations', 'organizations.id', '=', 'statuses.organization_id')
                ->join('users', 'organizations.user_id', '=', 'users.id')
                ->where('users.gender_id',$genderType->id)
                ->where('categorizations.model_type','like', '%Status')
                ->where('categorizations.segment_type', 'like','%TagType')
                ->whereIn('categorizations.segment_id', $tags ? $tags : $tags_types->pluck('id')->all());

            $item = $this->filterPeriod($item, $time_period);

            $gender_to_tags[] = $item->count();
        }

        return $gender_to_tags;
    }

    public function getAgeToUnsdgTags($time_period = false, $undsg_tags = []){
        $ages_variants = $this->ages_variants;
        $usndg_types = UnsdgTypes::all();

        if(in_array('Any', $undsg_tags)){
            $undsg_tags = false;
        }

        $age_to_undsgtags = [];
        foreach($ages_variants as $ages_variant){
            $item =  DB::connection('mysql_remote')->table('categorizations')
                ->join('statuses', 'statuses.id', '=', 'categorizations.model_id')
                ->join('organizations', 'organizations.id', '=', 'statuses.organization_id')
                ->join('users', 'organizations.user_id', '=', 'users.id')
                ->whereBetween('users.date_of_birth', [Carbon::now()->subYears($ages_variant[1]), Carbon::now()->subYears($ages_variant[0])])
                ->where('categorizations.model_type','like', '%Status')
                ->where('categorizations.segment_type', 'like','%UnsdgType')
                ->whereIn('categorizations.segment_id', $undsg_tags ? $undsg_tags : $usndg_types->pluck('id')->all());

            $item = $this->filterPeriod($item, $time_period);

            $age_to_undsgtags[] = $item->count();
        }

        return $age_to_undsgtags;
    }

    public function getAgeToTags($time_period = false, $tags = []){
        $ages_variants = $this->ages_variants;
        $tags_types = TagTypes::all();

        if(in_array('Any', $tags)){
            $tags = false;
        }

        $age_to_tags = [];
        foreach($ages_variants as $ages_variant){
            $item =  DB::connection('mysql_remote')->table('categorizations')
                ->join('statuses', 'statuses.id', '=', 'categorizations.model_id')
                ->join('organizations', 'organizations.id', '=', 'statuses.organization_id')
                ->join('users', 'organizations.user_id', '=', 'users.id')
                ->whereBetween('users.date_of_birth', [Carbon::now()->subYears($ages_variant[1]), Carbon::now()->subYears($ages_variant[0])])
                ->where('categorizations.model_type','like', '%Status')
                ->where('categorizations.segment_type', 'like','%UnsdgType')
                ->whereIn('categorizations.segment_id', $tags ? $tags : $tags_types->pluck('id')->all());

            $item = $this->filterPeriod($item, $time_period);

            $age_to_tags[] = $item->count();
        }

        return $age_to_tags;
    }

    public function filterPeriod($item ,$time_period, $table = 'statuses'){
        if($time_period){
            switch($time_period){
                case 'hour':
                    $item->whereDate($table.'.updated_at','>',Carbon::now()->subHour()->toDateString());
                    break;
                case 'day':
                    $item->whereDate($table.'.updated_at','>',Carbon::now()->subHours(24)->toDateString());
                    break;
                case 'month':
                    $item->whereDate($table.'.updated_at','>',Carbon::now()->subMonth()->toDateString());
                    break;
                case 'week':
                    $item->whereDate($table.'.updated_at','>',Carbon::now()->subWeek()->toDateString());
                    break;
                case 'custom':
                    if(request()->has(['time_from','time_to'])){
                        $item->whereBetween($table.'.updated_at', [date(request()->time_from), date(request()->time_to)]);
                    }
                    break;
            }
        }

        return $item;
    }

    public function getTopOrgTypes($organisation_type_id, $tags_type, $time_period = false, $tags = []){
        $types = $tags_type == 'UnsdgType' ? UnsdgTypes::all() : TagTypes::all();

        if(in_array('Any', $tags)){
            $tags = false;
        }

        $item = DB::connection('mysql_remote')->table('categorizations')
            ->join('statuses', 'statuses.id', '=', 'categorizations.model_id')
            ->join('organizations', 'organizations.id', '=', 'statuses.organization_id')
            ->where('organizations.organization_type_id',$organisation_type_id)
            ->where('categorizations.model_type','like', '%Status')
            ->where('categorizations.segment_type', 'like','%'.$tags_type)
            ->whereIn('categorizations.segment_id',$tags ? $tags : $types->pluck('id')->all());

        $item = $this->filterPeriod($item, $time_period);
        $arr = [];
        foreach($item->paginate(11) as $item_elem){
            if(isset($arr[$item_elem->title])){
                $arr[$item_elem->title]->count++;
            }else{
                $arr[$item_elem->title] = $item_elem;
                $arr[$item_elem->title]->count = 1;
            }
        }

        return $arr;
    }

    public function getTopTags($tags_type, $time_period = false){
        $table = [
            'UnsdgType' => 'unsdg_types',
            'TagType' => 'tag_types',
            'SnippetUnsdgType' => 'unsdg_types'
        ];
        $item = DB::connection('mysql_remote')->table('categorizations')
            ->join('statuses', 'statuses.id', '=', 'categorizations.model_id')
            ->join($table[$tags_type], $table[$tags_type].'.id', '=', 'categorizations.segment_id')
            ->where('categorizations.model_type','like', '%Status')
            ->where('categorizations.segment_type', 'like','%'.$tags_type);

        $item = $this->filterPeriod($item, $time_period);

        $arr = [];
        foreach($item->get() as $item_elem){
            if(isset($arr[$item_elem->title])){
                $arr[$item_elem->title]->count++;
            }else{
                $arr[$item_elem->title] = $item_elem;
                $arr[$item_elem->title]->count = 1;
            }
        }

        return array_slice($arr, 0,10);

    }

    public function getUsersByType($time_priod = false){
        $organisation_types =  OrganizationType::all();

        foreach ($organisation_types as $organisation_type){
            $item = Organization::where('organization_type_id', $organisation_type->id);

            $item = $this->filterPeriod($item, $time_priod, 'organizations');

            $arr[] = $item->count();
        }

        $item = User::leftJoin('organizations','organizations.user_id','users.id')
        ->where('organizations.user_id', null);

        $item = $this->filterPeriod($item, $time_priod, 'users');

        $arr[] = $item->count();
        return $arr;
    }

    public function getLoggedType($time_priod = false){
        $organisation_types =  array_flip(OrganizationType::all()->pluck('title')->toArray());
        foreach ($organisation_types as $key => $type){
            $organisation_types[$key] = 0;
        }
        $item = Session::where('organization_id','!=',null)->where('logout_at', null)->where('last_online_at','>', Carbon::now()->subMinutes(5));

        $item = $this->filterPeriod($item, $time_priod, 'sessions');

        $sessions_online = $item->get();

        $users_online = [];
        foreach($sessions_online as $session_online){
            if(!in_array($session_online->user_id, $users_online)){
                if($session_online->organization != null){
                    if(isset($organisation_types[$session_online->organization->organization_type->title])){
                        $organisation_types[$session_online->organization->organization_type->title]++;
                    }else{
                        $organisation_types[$session_online->organization->organization_type->title] = 1;
                    }
                }
                $users_online[] = $session_online->user_id;
            }
        }

        $item = Session::where('logout_at', null)->where('organization_id','=',null)->where('last_online_at','>', Carbon::now()->subMinutes(5));

        $item = $this->filterPeriod($item, $time_priod, 'sessions');
        $organisation_types['individual'] = 0;
        foreach($item->get() as $value){
            if(!in_array($value->user_id, $users_online)){
                $organisation_types['individual']++;
                $users_online[] = $value->user_id;
            }
        }
        $arr = [];
        foreach($organisation_types as $val){
            $arr[] = $val;
        }


        return $arr;
    }

    public function getAllUsers($time_priod = false){
        $item = $this->filterPeriod(User::where('id','>',0), $time_priod, 'users');

        return $item->count();
    }
    public function getAllUsersOnline($time_priod = false){
        $item = Session::where('logout_at', null)->where('last_online_at','>', Carbon::now()->subMinutes(5));

        $item = $this->filterPeriod($item, $time_priod, 'sessions');

        return count(array_unique($item->pluck('user_id')->toArray()));
    }

    public function getAllPosts($post_type = null, $time_priod = false){

        if($post_type){
            $item = Status::select('statuses.id as id')->join('categorizations','statuses.id', '=', 'categorizations.model_id')
                ->where('categorizations.segment_type','like','%' . $post_type);
        }else{
            $item = Status::where('id','>',0);
        }
        $item = $this->filterPeriod($item, $time_priod);

        return count(array_unique($item->pluck('id')->toArray()));
    }

    public function getIndustryToPost($industryClass, $time_priod = false){
        $industriestoPosts = [
            'labels' => [],
            'values' => []
        ];
        $industry_type = 'organizations.industry_type_id';
        $industry_table = 'industry_types';
        switch($industryClass){
            case IndustryType::class:
                $industry_type = 'organizations.industry_type_id';
                $industry_table = 'industry_types';
                break;
            case IndustrySubtype::class:
                $industry_type = 'organizations.industry_subtype_id';
                $industry_table = 'industry_subtypes';
                break;
        }
        $item = Status::join('organizations', 'organizations.id', '=', 'statuses.organization_id')
            ->LeftJoin($industry_table, 'organizations.industry_type_id','=',$industry_table.'.id')
            ->whereIn($industry_type, $industryClass::pluck('id'));
        $item = $this->filterPeriod($item, $time_priod);

        $industry_arr = [];
        foreach($item->get() as $status){
            if(!isset($industry_arr[$status->title])){
                $industry_arr[$status->title] = 1;
            }
            $industry_arr[$status->title]++;
        }

        foreach($industry_arr as  $key => $val){
            $industriestoPosts['labels'][] = ('"'.$key.'"');
            $industriestoPosts['values'][] = $val;
        }


        return $industriestoPosts;
    }

    public function getIndustryToUser($industryClass, $time_priod = false){
        $industriestoPosts = [
            'labels' => [],
            'values' => []
        ];
        $industry_type = 'industry_types';
        switch($industryClass){
            case IndustryType::class:
                $industry_type = 'industry_types';
                break;
            case IndustrySubtype::class:
                $industry_type = 'industry_subtypes';
                break;
        }
        $item =  User::LeftJoin('organizations', 'organizations.user_id', '=', 'users.id')
            ->LeftJoin($industry_type, 'organizations.industry_type_id','=',$industry_type.'.id')
            ->whereIn('organizations.industry_type_id', $industryClass::pluck('id'));
        $item = $this->filterPeriod($item, $time_priod, 'users');
        $industry_arr = [];
        foreach($item->get() as $user){
            if(!isset($industry_arr[$user->title])){
                $industry_arr[$user->title] = 1;
            }
            $industry_arr[$user->title]++;
        }

        foreach($industry_arr as $key => $val){
            $industriestoPosts['labels'][] = ('"'.$key.'"');
            $industriestoPosts['values'][] = $val;
        }
        return $industriestoPosts;
    }

    public function versionone()
    {
        $organisation_types =  OrganizationType::all();
        $usndg_types = UnsdgTypes::all();


        $args = [
            'gender_types' => Gender::all(),
            'unsdg_types' => $usndg_types,
            'tag_types' => TagTypes::all(),
            'ages_variants' => $this->ages_variants,
            'organisation_types' => $organisation_types,
            'organisation_types_to_unsdg_tags' => $this->getOrgTypesToUnsdgTags(),
            'organisation_types_to_tags' => $this->getOrgTypesToTags(),
            'gender_to_unsdg_tags' => $this->getGenderToUnsdgTags(),
            'gender_to_tags' => $this->getGenderToTags(),
            'age_to_undsg_tags' => $this->getAgeToUnsdgTags(),
            'age_to_tags' => $this->getAgeToTags(),
            'top_10_companies_undsg_tags' => $this->getTopOrgTypes(1, 'UnsdgType'),
            'top_10_companies_tags' => $this->getTopOrgTypes(1, 'TagType'),
            'top_10_nonpofits_undsg_tags' => $this->getTopOrgTypes(2, 'UnsdgType'),
            'top_10_nonpofits_tags' => $this->getTopOrgTypes(2, 'TagType'),
            'top_10_unsdg_tags' => $this->getTopTags('UnsdgType'),
            'top_10_tags' => $this->getTopTags('TagType'),
            'top_10_snippets' => $this->getTopTags('SnippetUnsdgType'),
            'user_registered_by_type' => $this->getUsersByType(),
            'user_logged_by_type' => $this->getLoggedType(),
            'all_users' => $this->getAllUsers(),
            'all_users_online' => $this->getAllUsersOnline(),
            'all_posts' => $this->getAllPosts(),
            'all_posts_unsdg' => $this->getAllPosts( 'UnsdgType'),
            'all_posts_with_tag' => $this->getAllPosts('TagType' ),
            'all_posts_with_snippet' => $this->getAllPosts('SnippetUnsdgType'),
            'industry_types_to_posts' => $this->getIndustryToPost(IndustryType::class),
            'industry_types_to_users' => $this->getIndustryToUser(IndustryType::class),
            'subindustry_types_to_posts' => $this->getIndustryToPost(IndustrySubtype::class),
            'subindustry_types_to_users' => $this->getIndustryToUser(IndustrySubtype::class),
        ];


        return view('dashboard.v1', $args);
    }

    public function DashboardCharts(Request $request){
        $organisation_types =  OrganizationType::all();
        $genderTypes = Gender::all();
        $age_labels = [];
        foreach($this->ages_variants as $age_variant){
            $age_labels[] = $age_variant[0].' - '.$age_variant[1];
        }
        $industry_to_post = $this->getIndustryToPost(IndustryType::class, $request->time_period);
        $industry_to_user = $this->getIndustryToUser(IndustryType::class, $request->time_period);
        $subindustry_to_post = $this->getIndustryToPost(IndustrySubtype::class, $request->time_period);
        $subindustry_to_user = $this->getIndustryToUser(IndustrySubtype::class, $request->time_period);
        return [
            'undsg_tags' => ['data' => $this->getOrgTypesToUnsdgTags($request->time_period, $request->undsg_tags),'labels' => $organisation_types->pluck('title')->merge('Individual')],
            'tags' => ['data' => $this->getOrgTypesToTags($request->time_period, $request->tags),'labels' => $organisation_types->pluck('title')->merge('Individual')],
            'gender_to_unsdg_tags' => ['data' => $this->getGenderToUnsdgTags($request->time_period, $request->undsg_tags),'labels' => $genderTypes->pluck('title')],
            'gender_to_tags' => ['data' => $this->getGenderToTags($request->time_period, $request->tags),'labels' => $genderTypes->pluck('title')],
            'age_to_undsg_tags' => ['data' => $this->getAgeToUnsdgTags($request->time_period, $request->undsg_tags),'labels' => $age_labels],
            'age_to_tags' => ['data' => $this->getAgeToTags($request->time_period, $request->tags),'labels' => $age_labels],
            'top_10_companies_undsg_tags' => $this->getTopOrgTypes(1, 'UnsdgType',$request->time_period, $request->tags),
            'top_10_companies_tags' => $this->getTopOrgTypes(1, 'TagType',$request->time_period, $request->tags),
            'top_10_nonpofits_undsg_tags' => $this->getTopOrgTypes(2, 'UnsdgType',$request->time_period, $request->tags),
            'top_10_nonpofits_tags' => $this->getTopOrgTypes(2, 'TagType',$request->time_period, $request->tags),
            'top_10_unsdg_tags' => $this->getTopTags('UnsdgType',$request->time_period),
            'top_10_tags' => $this->getTopTags('TagType',$request->time_period),
            'top_10_snippets' => $this->getTopTags('SnippetUnsdgType',$request->time_period),
            'user_types' =>  ['data' => $this->getUsersByType($request->time_period), 'labels' => $organisation_types->pluck('title')->merge('Individual')],
            'online_types' =>[ 'data' => $this->getLoggedType($request->time_period), 'labels' => $organisation_types->pluck('title')->merge('Individual')],
            'all_users' => $this->getAllUsers($request->time_period),
            'all_users_online' => $this->getAllUsersOnline($request->time_period),
            'all_posts' => $this->getAllPosts(null, $request->time_period),
            'all_posts_unsdg' => $this->getAllPosts('UnsdgType', $request->time_period ),
            'all_posts_with_tag' => $this->getAllPosts('TagType', $request->time_period ),
            'all_posts_with_snippet' => $this->getAllPosts('SnippetUnsdgType', $request->time_period ),
            'industry_types_to_posts' => ['data' => $industry_to_post['values'], 'labels' => $industry_to_post['labels']],
            'industry_types_to_users' =>['data' => $industry_to_user['values'], 'labels' => $industry_to_user['labels']] ,
            'subindustry_types_to_posts' => ['data' => $subindustry_to_post['values'], 'labels' => $subindustry_to_post['labels']],
            'subindustry_types_to_users' => ['data' => $subindustry_to_user['values'], 'labels' => $subindustry_to_user['labels']],
        ];
    }
    public function versiontwo()
    {
        return view('dashboard.v2');
    }
    public function versionthree()
    {
        return view('dashboard.v3');
    }
}
