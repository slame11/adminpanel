<?php

namespace App\Http\Controllers;

use App\Message;
use App\WokenUpModels\User;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index()
    {
        return view('messages.index', [
            'title' => __('general.messages'),
            'messages' => Message::paginate()
        ]);
    }

    public function save(Request $request)
    {
        //When new changes will be merged
        /*
              $endpoint = env("WOKENUP_REMOTE")."/api/portal-notification-all?title={$request->title}&description={$request->message}";
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $endpoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
         */

        $failed_query = 0;


        foreach(User::all() as $user){
            try{
                $endpoint = env("WOKENUP_REMOTE")."/api/portal-notification/{$user->id}?title={$request->title}&description={$request->message}";
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $endpoint,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_TIMEOUT => 30000,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        // Set Here Your Requesred Headers
                        'Content-Type: application/json',
                    ),
                ));
                $response = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);
            }catch(\Exception $exception){
                $failed_query++;
            }
        }

        Message::create($request->only(['message','title']));

        return redirect()
            ->route('messages.index')
            ->with('message.level', 'success')
            ->with('message.content', 'Message successfully sent.');
    }

    public function delete(Request $request){
        if($request->id){
            Message::find($request->id)->delete();
        }

        return back();
    }
}
