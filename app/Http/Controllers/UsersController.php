<?php

namespace App\Http\Controllers;

use App\WokenUpModels\StatusesReport;
use App\User;
use App\WokenUpModels\User as WokenUpUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WokenUpModels\Country;
use App\WokenUpModels\Gender;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = new WokenUpUser();
        if ($request->age) {
            $users = $users->whereYear('date_of_birth', Carbon::now()->subYears($request->age)->year);
        }

        if ($request->gender) {
            $users = $users->where('gender_id', $request->gender);
        }

        if ($request->created_from) {
            $users = $users->where('created_at','>=', $request->created_from);
        }

        if ($request->created_to) {
            $users = $users->where('created_at','<=', $request->created_to);
        }

        if ($request->location) {
            $users = $users->where('location', 'like', '%'.$request->input('location').'%');
        }

        if ($request->order && $request->sort) {
            $users = $request->order == 'user'
                ? $users->orderBy('firstname', $request->sort)->orderBy('lastname', $request->sort)
                : $users->orderBy($request->order, $request->sort);
        } else {
            $users = $users->orderBy('id', 'DESC');
        }

        return view('user.index', [
            'title' => __('general.users'),
            'users' => $users->paginate(),
            'genders' => Gender::all(),
            'request' => collect($request->all()),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        return view('user.create', [
            'countries' => Country::orderBy('name')->get()->pluck('name', 'id'),
            'genders' => Gender::orderBy('title')->get()->pluck('title', 'id'),
            'user' => new WokenUpUser,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|email|max:255',
            'password' => 'required|min:8|confirmed',
        ]);

        $arr = $request->all();
        $arr['password'] = Hash::make($arr['password']);
        WokenUpUser::create($arr);

        return redirect()
            ->route('users.index')
            ->with('message.level', 'success')
            ->with('message.content', 'User successfully added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('user.edit', [
            'countries' => Country::orderBy('name')->get()->pluck('name', 'id'),
            'genders' => Gender::orderBy('title')->get()->pluck('title', 'id'),
            'user' => WokenUpUser::where('id', $id)->firstOrFail(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WokenUpUser $user)
    {

        $request->validate([
            'email' => 'required|email|max:255',
            'password' => 'confirmed',
        ]);
        $user->fill($request->except('password'));

        if ($request['password'] != '') {
            $user->password = Hash::make($request['password']);
        }

        $user->save();

        return redirect()
            ->route('users.index')
            ->with('message.level', 'success')
            ->with('message.content', 'User successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(WokenUpUser $user)
    {
        $user->friends()->sync([]);
        $user->loveLikes()->delete();
        $user->media()->delete();
        $user->organizations()->delete();
        $user->session()->delete();
        $user->help_message()->delete();
        StatusesReport::whereIn('status_id',$user->statuses()->pluck('id'))->delete();
        $user->statuses()->delete();
        $user->delete();

        return redirect()
            ->route('users.index')
            ->with('message.level', 'success')
            ->with('message.content', 'User successfully deleted.');
    }
}
