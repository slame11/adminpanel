<?php

namespace App\WokenUpModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class User extends Model
{
    use Notifiable;

    protected $connection = 'mysql_remote';
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'firstname',
        'lastname',
        'date_of_birth',
        'address',
        'country_id',
        'about',
        'about_extra',
        'employer',
        'phone',
        'job',
        'school',
        'gender_id',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function friends()
    {
        return $this->belongsToMany(User::class, 'friendships', 'requester', 'user_requested', 'id', 'id');
    }

    public function loveLikes()
    {
        return $this->hasMany(LoveLike::class, 'user_id', 'id');
    }

    public function media()
    {
        return $this->morphMany(Media::class, 'model');
    }

    public function organizations()
    {
        return $this->hasMany(Organization::class, 'user_id', 'id');
    }

    public function statuses()
    {
        return $this->hasMany(Status::class, 'user_id', 'id');
    }

    public function session()
    {
        return $this->hasMany('App\WokenUpModels\Session');
    }

    public function help_message()
    {
        return $this->hasMany('App\WokenUpModels\HelpMessage');
    }

    public function getFullnameAttribute()
    {
        return $this->firstname . ' ' . $this->lastname;
    }
}
