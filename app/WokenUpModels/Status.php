<?php

namespace App\WokenUpModels;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $connection = 'mysql_remote';
    protected $table = 'statuses';
    protected $fillable = [
        'user_id', 'organization_id', 'view_type_id', 'parent_id', 'body', 'snippet_text', 'snippet_number'
    ];

    public function user()
    {
        return $this->belongsTo('App\WokenUpModels\User');
    }

    public function organization()
    {
        return $this->belongsTo('App\WokenUpModels\Organization');
    }

    public function setViewTypeIdAttribute($value)
    {
        $this->attributes['view_type_id'] = $value ?? 2;
    }

    public function reports(){
        return $this->hasMany('App\WokenUpModels\StatusReport','status_id', 'id');
    }
}
