<?php

namespace App\WokenUpModels;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $connection = 'mysql_remote';
    protected $table = 'sessions';

    public function user()
    {
        return $this->belongsTo('App\WokenUpModels\User');
    }

    public function organization()
    {
        return $this->belongsTo('App\WokenUpModels\Organization');
    }
}
