<?php

namespace App\WokenUpModels;

use Illuminate\Database\Eloquent\Model;

class IndustryType extends Model
{
    protected $connection = 'mysql_remote';
    protected $fillable = [
        'title',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /*protected $with = [
        'subtypes'
    ];*/


    public function subtypes() {
        return $this->hasMany('App\Models\IndustrySubtype');
    } 

}
