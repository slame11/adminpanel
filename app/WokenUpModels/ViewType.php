<?php

namespace App\WokenUpModels;

use Illuminate\Database\Eloquent\Model;

class ViewType extends Model
{
    protected $connection = 'mysql_remote';
    protected $table = 'view_types';
}
