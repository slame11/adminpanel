<?php

namespace App\WokenUpModels;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $connection = 'mysql_remote';
    protected $table = 'countries';
}
