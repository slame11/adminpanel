<?php

namespace App\WokenUpModels;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    protected $connection = 'mysql_remote';
    protected $table = 'genders';
}
