<?php

namespace App\WokenUpModels;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $connection = 'mysql_remote';
    protected $table = 'media';
}
