<?php

namespace App\WokenUpModels;

use Illuminate\Database\Eloquent\Model;

class IndustrySubtype extends Model
{
    protected $connection = 'mysql_remote';

    protected $fillable = [
		'industry_type_id',
        'title',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
