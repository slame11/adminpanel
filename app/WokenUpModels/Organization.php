<?php

namespace App\WokenUpModels;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $connection = 'mysql_remote';
    protected $table = 'organizations';
    protected $fillable = [
        'user_id', 'is_premium', 'title', 'website_url', 'about', 'about_extra', 'country_id', 'organization_type_id', 'organization_size_id', 'industry_type_id', 'industry_subtype_id', 'address', 'address_lat', 'address_lon', 'messaging', 'user_acceptance', 'stripe_id', 'stripe_active', 'stripe_subscription', 'subscription_end_at'
    ];

    public function organization_type()
    {
        return $this->belongsTo('App\WokenUpModels\OrganizationType');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
