<?php

namespace App\WokenUpModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Notifications\HelpMessage as HelpMessageNotification;
//use App\Notifications\UserHelpMessage as UserHelpMessageNotification;

use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Illuminate\Support\Str;

class HelpMessage extends Model
{

    use Notifiable;
	protected $guarded = ['id'];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];


    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
