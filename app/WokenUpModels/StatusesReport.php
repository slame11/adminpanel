<?php

namespace App\WokenUpModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class StatusesReport extends Model
{
    protected $connection = 'mysql_remote';
    protected $guarded = [
        'id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

}
