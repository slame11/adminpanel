<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Auth::routes();


Route::middleware('auth')->group(function () {
    Route::prefix('dashboard')->group(function () {
        Route::get('home', 'DashboardController@versionone')->name('home');
        Route::get('v2', 'DashboardController@versiontwo')->name('v2');
        Route::get('v3', 'DashboardController@versionthree')->name('v3');

        Route::post('dashboard_charts', 'DashboardController@DashboardCharts')->name('dashboard_charts');
    });

    Route::prefix('messages')->as('messages.')->group(function () {
        Route::get('/', 'MessageController@index')->name('index');
        Route::post('save', 'MessageController@save')->name('save');
        Route::post('delete', 'MessageController@delete')->name('delete');
    });

    Route::resource('organizations', 'OrganizationController');
    Route::resource('statuses', 'StatusesController');
    Route::resource('admins', 'AdminController');
    Route::resource('users', 'UsersController');
});


Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
