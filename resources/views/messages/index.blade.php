@extends('layouts.master')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>@lang('general.messages')</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">@lang('general.home')</a></li>
                        <li class="breadcrumb-item active">@lang('general.messages')</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">@lang('general.messages')</h3>
            </div>
            <div class="card-body">
                <form id="message_form" action="{{ route('messages.save') }}" method="post">
                    @csrf
                    <div class="card mt-3">
                        <div class="card-header">
                            <h3 class="card-title">Send message</h3>
                        </div>
                        <div class="card-body">

                            <div class="form-group">
                                <label class="col-form-label text-right">Title</label>
                                <input name="title" type="text" class="form-control" required />
                            </div>

                            <div class="form-group">
                                <label class="col-form-label text-right">Message</label>
                                <textarea name="message" class="form-control" required></textarea>
                            </div>

                            <div class="text-right">
                                <button type="submit" class="btn btn-primary ld-ext-right" id="btn_sent">
                                    Send
                                    <div class="ld ld-ring ld-spin"></div>
                                </button>
                            </div>

                        </div>
                    </div>

                </form>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Sent messages</h3>
                    </div>
                    <div class="card-body">
                        <table class="table responsive table-hover table-striped table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Message</th>
                            <th>Created_at</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($messages as $item)

                        <tr>
                            <td data-label="Title">{{ $item->title }}</td>
                            <td data-label="Message">{{ $item->message }}</td>
                            <td data-label="Created_at">{{ $item->created_at }}</td>
                            <td>
                                <form class="float-sm-left ml-2 d-inline-block d-sm-inline" onsubmit="if(confirm('@lang('general.deleting')')){ return true }else{ return false }" action="{{ route('messages.delete') }}" method="post">
                                    <input type="hidden" name="id" value="{{$item->id}}" />
                                    {{csrf_field()}}
                                    <button type="submit" class="btn-sm remove_item btn btn-danger pull-left"><i class="fa fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="5" class="text-center">
                                <b>@lang('general.empty_data')</b>
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
                    </div>
                </div>
                <div class="col-sm-12 text-center">
                    <nav aria-label="page navigation" class="justify-content-center mt-3">
                        {{ $messages->appends(request()->except('page'))->links() }}
                    </nav>
                </div>

            </div>
        </div>

    </section>
</div>
@endsection

@section('javascript')
    <script>
        $(function () {
            $('#message_form').on('submit',function () {
                $(this).find('button[type=submit]').addClass('running');
            })
        })
    </script>
    @endsection