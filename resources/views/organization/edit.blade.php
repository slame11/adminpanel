@extends('layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>@lang('Edit organization')</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('general.dashboard')</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('organizations.index') }}">@lang('general.organizations')</a></li>
                            <li class="breadcrumb-item active">@lang('general.edit')</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            {!! Form::model($organization, ['route' => [
                'organizations.update', $organization],
                'method' => 'put',
                'files' => true,
                'id' => 'organization_create_form'
            ]) !!}
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">@lang('general.organizations')</h3>
                    <div class="card-tools">
                        {!! Form::submit(__('general.save'), ['class' => 'btn btn-success', 'form' => 'organization_create_form']) !!}
                    </div>
                </div>
                <div class="card-body">
                    @include('organization.partials.form')
                    {!! Form::hidden('modified_by', Auth::id()) !!}
                </div>
            </div>

        </section>
    </div>
@endsection
