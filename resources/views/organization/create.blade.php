@extends('layouts.master')


@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>@lang('Organization creating')</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i>@lang('general.dashboard')</a></li>
                            <li><a href="{{ route('organizations.index') }}"><i class="fa fa-thumbs-up"></i>@lang('general.organizations')</a></li>
                            <li class="active"><i class="fa fa-thumbs-up"></i>@lang('general.edit')</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            {!! Form::model($organization, [
                'route' => ['organizations.store'],
                'method' => 'post',
                'files' => true,
                'id' => 'organization_create_form'
            ]) !!}
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">@lang('general.organizations')</h3>
                    <div class="card-tools">
                        {!! Form::submit(__('general.save'), ['class' => 'btn btn-success', 'form' => 'organization_create_form']) !!}
                    </div>
                </div>
                <div class="card-body">
                    @include('organization.partials.form')
                    {!! Form::hidden('created_by', Auth::id()) !!}
                </div>
            </div>

        </section>
    </div>
    <div class="row">
        <div class="col-md-12">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    <div class="pull-right">
                        {!! Form::submit(__('general.save'), ['class' => 'btn btn-success']) !!}

                        <a href="{{ route('organizations.index') }}" class="btn btn-default"><i class="fa fa-share"></i></a>
                    </div>
                </div>
                <div class="box-body">

                </div>
            </div>

        </div>
    </div>
@endsection
