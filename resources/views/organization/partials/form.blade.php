<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12 ">
            <div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
                {!! Form::label('user_id', __('general.user')) !!}
                {!! Form::select('user_id', $users, $organization->user_id, [
                    'class' => 'form-control',  
                ]) !!}
                {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                {!! Form::label('title', __('general.title')) !!}
                {!! Form::text('title', $organization->title, [
                    'class' => 'form-control',
                    'placeholder' => __('general.title'),
                    'size' => 40,
                    'required'=>'true'
                ]) !!}
                {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('website_url') ? 'has-error' : ''}}">
                {!! Form::label('website_url', __('general.website_url')) !!}
                {!! Form::text('website_url', $organization->website_url, [
                    'class' => 'form-control',
                    'placeholder' => __('general.website_url'),
                    'size' => 40,
                ]) !!}
                {!! $errors->first('website_url', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('country_id') ? 'has-error' : ''}}">
                {!! Form::label('country_id', __('general.country')) !!}
                {!! Form::select('country_id', $countries, $organization->country_id, [
                    'class' => 'form-control',
                ]) !!}
                {!! $errors->first('country_id', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('organization_type_id') ? 'has-error' : ''}}">
                {!! Form::label('organization_type_id', __('Organization type')) !!}
                {!! Form::select('organization_type_id', $organization_types, $organization->organization_type_id, [
                    'class' => 'form-control',
                ]) !!}
                {!! $errors->first('organization_type_id', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('organization_size_id') ? 'has-error' : ''}}">
                {!! Form::label('organization_size_id', __('Organization size')) !!}
                {!! Form::select('organization_size_id', $organization_sizes, $organization->organization_size_id, [
                    'class' => 'form-control',
                ]) !!}
                {!! $errors->first('organization_size_id', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('industry_type_id') ? 'has-error' : ''}}">
                {!! Form::label('industry_type_id', __('Industry type')) !!}
                {!! Form::select('industry_type_id', $industry_types, $organization->industry_type_id, [
                    'class' => 'form-control',
                ]) !!}
                {!! $errors->first('industry_type_id', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('industry_subtype_id') ? 'has-error' : ''}}">
                {!! Form::label('industry_subtype_id', __('Industry subtype')) !!}
                {!! Form::select('industry_subtype_id', $industry_subtypes, $organization->industry_subtype_id, [
                    'class' => 'form-control',
                ]) !!}
                {!! $errors->first('industry_subtype_id', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('about') ? 'has-error' : ''}}">
                {!! Form::label('about', __('general.about')) !!}
                {!! Form::textarea('about',$organization->about,[
                    'class' => 'form-control',
                    'placeholder' => __('general.about'),
                ]) !!}
                {!! $errors->first('about', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('about_extra') ? 'has-error' : ''}}">
                {!! Form::label('about_extra', __('general.about_extra')) !!}
                {!! Form::textarea('about_extra',$organization->about_extra,[
                    'class' => 'form-control',
                    'placeholder' => __('general.about_extra'),
                ]) !!}
                {!! $errors->first('about_extra', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                {!! Form::label('address', __('general.address')) !!}
                {!! Form::textarea('address',$organization->address,[
                    'class' => 'form-control',
                    'placeholder' => __('general.address'),
                ]) !!}
                {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('address_lat') ? 'has-error' : ''}}">
                {!! Form::label('address_lat', __('Address latency')) !!}
                {!! Form::number('address_lat', $organization->address_lat,[
                    'class' => 'form-control',
                    'placeholder' => __('Address latency'),
                    'size' => 20,
                ]) !!}
                {!! $errors->first('address_lat', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('address_lon') ? 'has-error' : ''}}">
                {!! Form::label('address_lon', __('Address longitude')) !!}
                {!! Form::number('address_lon', $organization->address_lon,[
                    'class' => 'form-control',
                    'placeholder' => __('Address longitude'),
                    'size' => 20,
                ]) !!}
                {!! $errors->first('address_lon', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('stripe_id') ? 'has-error' : ''}}">
                {!! Form::label('stripe_id', __('general.stripe_id')) !!}
                {!! Form::text('stripe_id', $organization->stripe_id, [
                    'class' => 'form-control',
                    'placeholder' => __('general.stripe_id'),
                    'size' => 40,
                ]) !!}
                {!! $errors->first('stripe_id', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('stripe_subscription') ? 'has-error' : ''}}">
                {!! Form::label('stripe_subscription', __('general.stripe_subscription')) !!}
                {!! Form::text('stripe_subscription', $organization->stripe_subscription, [
                    'class' => 'form-control',
                    'placeholder' => __('general.stripe_subscription'),
                    'size' => 40,
                ]) !!}
                {!! $errors->first('stripe_subscription', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('subscription_end_at') ? 'has-error' : ''}}">
                {!! Form::label('subscription_end_at', __('general.subscription_end_at')) !!}
                {!! Form::date('subscription_end_at',$organization->subscription_end_at, [
                    'class' => 'form-control',
                    'placeholder' => __('general.subscription_end_at'),
                ]) !!}
                {!! $errors->first('subscription_end_at', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="checkbox icheck">
                <label>
                    <input type="checkbox" name="is_premium" value="1" {{ $organization->is_premium ? 'checked' : '' }}> Is premium
                </label>
            </div>

            <div class="checkbox icheck">
                <label>
                    <input type="checkbox" name="messaging" value="1" {{ $organization->messaging ? 'checked' : '' }}> Messaging
                </label>
            </div>

            <div class="checkbox icheck">
                <label>
                    <input type="checkbox" name="user_acceptance" value="1" {{ $organization->user_acceptance ? 'checked' : '' }}> User acceptable
                </label>
            </div>

            <div class="checkbox icheck">
                <label>
                    <input type="checkbox" name="stripe_active" value="1" {{ $organization->stripe_active ? 'checked' : '' }}> Stripe active
                </label>
            </div>
        </div>
    </div>
</div>