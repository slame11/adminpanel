@extends('layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>@lang('general.organizations')</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">@lang('general.home')</a></li>
                            <li class="breadcrumb-item active">@lang('general.organizations')</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">@lang('general.organizations')</h3>

                    <div class="card-tools">
                        <a href="{{ route('organizations.create') }}" class="btn btn-sm btn-success add_new_organization">
                            @lang('general.new')
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-hover table-striped table-bordered table-condensed responsive">
                        <thead>
                        <tr>
                            @foreach (['title', 'type', 'user'] as $column)
                            <th>
                                <a href="{{ route('organizations.index', $request->merge([
                                    'sort' => request()->order == $column && request()->sort == 'ASC' ? 'DESC' : 'ASC',
                                    'order' => $column
                                ])->all()) }}">
                                    @lang('general.' . $column)
                                </a>
                            </th>
                            @endforeach
                            <th>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                            @forelse($organizations as $organization)
                            <tr>
                                <td data-label="@lang('general.title')">{{ $organization->title }}</td>
                                <td data-label="@lang('general.type')">{{ $organization->organization_type->title }}</td>
                                <td data-label="@lang('general.user')">{{ $organization->user->fullname }}</td>
                                <td class="td-action">
                                    <a href="{{ route('organizations.edit', $organization) }}" class="float-sm-left d-inline-block d-sm-inline btn btn-success btn-sm pull-left">
                                        <i class="fas fa-pencil-alt"></i>
                                    </a>

                                    <form class="float-sm-left ml-2 d-inline-block d-sm-inline" onsubmit="if(confirm('@lang('general.deleting')')){ return true }else{ return false }" action="{{ route('organizations.destroy', $organization) }}" method="post">
                                        <input type="hidden" name="_method" value="delete" />
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn-sm remove_item btn btn-danger pull-left"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5" class="text-center">
                                    <b>@lang('general.empty_data')</b>
                                </td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    <div class="col-sm-12 text-center">
                        <nav aria-label="page navigation" class="justify-content-center mt-3">
                            {{ $organizations->appends(request()->except('page'))->links() }}
                        </nav>
                    </div>

                </div>
            </div>

        </section>
    </div>
@endsection