<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12 ">
            <input type="hidden" name="avatar_id" id="avatar-id" value="{{ old('avatar_id') ?? $user->avatar_id }}">
            <input type="hidden" name="avatar_name" id="avatar-name" value="">
            <input type="hidden" name="avatar_path" id="avatar-path" value="">

            <div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
                {!! Form::label('username', __('general.username')) !!}
                {!! Form::text('username', $user->username, [
                    'class' => 'form-control',
                    'placeholder' => __('general.username'),
                    'size' => 40,
                    'required'=>'true'
                ]) !!}
                {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('firstname') ? 'has-error' : ''}}">
                {!! Form::label('firstname', __('general.firstname')) !!}
                {!! Form::text('firstname', $user->firstname, [
                    'class' => 'form-control',
                    'placeholder' => __('general.firstname'),
                    'size' => 40,
                    'required'=>'true'
                ]) !!}
                {!! $errors->first('firstname', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('lastname') ? 'has-error' : ''}}">
                {!! Form::label('lastname', __('general.lastname')) !!}
                {!! Form::text('lastname', $user->lastname, [
                    'class' => 'form-control',
                    'placeholder' => __('general.lastname'),
                    'size' => 40,
                    'required'=>'true'
                ]) !!}
                {!! $errors->first('lastname', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('email', __('general.email')) !!}
                {!! Form::text('email',$user->email, [
                    'class' => 'form-control',
                    'placeholder' => __('general.email'),
                    'size' => 70,
                    'required'=>'true'
                ]) !!}
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('date_of_birth') ? 'has-error' : ''}}">
                {!! Form::label('date_of_birth', __('general.date_of_birth')) !!}
                {!! Form::date('date_of_birth',$user->date_of_birth, [
                    'class' => 'form-control',
                    'placeholder' => __('general.date_of_birth'),
                ]) !!}
                {!! $errors->first('date_of_birth', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('country_id') ? 'has-error' : ''}}">
                {!! Form::label('country_id', __('general.country')) !!}
                {!! Form::select('country_id', $countries, $user->country_id, [
                    'class' => 'form-control',
                    'placeholder' => __('general.country'),
                ]) !!}
                {!! $errors->first('country_id', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                {!! Form::label('address', __('general.address')) !!}
                {!! Form::text('address',$user->address,[
                    'class' => 'form-control',
                    'placeholder' => __('general.address'),
                    'size' => 255,
                ]) !!}
                {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
                {!! Form::label('phone', __('general.telephone')) !!}
                {!! Form::text('phone',$user->phone,[
                    'class' => 'form-control',
                    'placeholder' => __('general.telephone'),
                    'size' => 20,
                ]) !!}
                {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group">
                <label for="">@lang('general.password')</label>
                <div class="input-group {{ $errors->has('password') ? 'has-error' : ''}}">
                    <input type="password" class="form-control" name="password" placeholder="@lang('general.password')" size="255"
                        data-character-set='a-z,A-Z,0-9' rel='gp' data-size='16' {{ !$user->id ? 'required' : '' }} />
                    <span class="input-group-btn" id="hashpassword">
                        <button type="button" class="btn btn-default getNewPass">
                            <i class="fas fa-sync"></i>
                        </button>
                    </span>
                </div>
                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group">
                <label for="">@lang('general.password_confirm')</label>
                <input type="password" class="form-control" name="password_confirmation" placeholder="@lang('general.repeat password')" size="255" rel="gp_confirm" />
            </div>

            <div class="form-group {{ $errors->has('employer') ? 'has-error' : ''}}">
                {!! Form::label('employer', __('general.employer')) !!}
                {!! Form::text('employer',$user->employer,[
                    'class' => 'form-control',
                    'placeholder' => __('general.employer'),
                    'size' => 255,
                ]) !!}
                {!! $errors->first('employer', '<p class="help-block">:message</p>') !!}
            </div>
            
            <div class="form-group {{ $errors->has('job') ? 'has-error' : ''}}">
                {!! Form::label('job', __('general.job')) !!}
                {!! Form::text('job',$user->job,[
                    'class' => 'form-control',
                    'placeholder' => __('general.job'),
                    'size' => 255,
                ]) !!}
                {!! $errors->first('job', '<p class="help-block">:message</p>') !!}
            </div>
            
            <div class="form-group {{ $errors->has('gender_id') ? 'has-error' : ''}}">
                {!! Form::label('gender_id', __('general.gender')) !!}
                {!! Form::select('gender_id', $genders, $user->gender_id,[
                    'class' => 'form-control',
                    'placeholder' => __('general.gender'),
                ]) !!}
                {!! $errors->first('gender_id', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('about') ? 'has-error' : ''}}">
                {!! Form::label('about', __('general.about')) !!}
                {!! Form::textarea('about',$user->about,[
                    'class' => 'form-control',
                    'placeholder' => __('general.about'),
                ]) !!}
                {!! $errors->first('about', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('about_extra') ? 'has-error' : ''}}">
                {!! Form::label('about_extra', __('general.about_extra')) !!}
                {!! Form::textarea('about_extra',$user->about_extra,[
                    'class' => 'form-control',
                    'placeholder' => __('general.about_extra'),
                ]) !!}
                {!! $errors->first('about_extra', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

    </div>

    <div class="col-sm-6"></div>
</div>

{{--@include('admin.user.partials.translations')--}}