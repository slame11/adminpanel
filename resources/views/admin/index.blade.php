@extends('layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>@lang('general.users')</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">@lang('general.home')</a></li>
                            <li class="breadcrumb-item active">@lang('general.admins')</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">@lang('general.users')</h3>

                    <div class="card-tools">
                        <a href="{{ route('admins.create') }}" class="btn btn-sm btn-success add_new_user">
                            @lang('general.new')
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <form class="" action="" method="get">

                        @if (request()->page)
                            <input type="hidden" value="{{ request()->page }}" name="page" />
                        @endif

                        @if (request()->sort)
                            <input type="hidden" value="{{ request()->sort }}" name="sort" />
                        @endif

                        @if (request()->order)
                            <input type="hidden" value="{{ request()->order }}" name="order" />
                        @endif

                        <div class="card mt-3">
                            <div class="card-header">
                                <h3 class="card-title">Filter</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-4 text-right" >Profile registered</label>
                                            <div class="col-sm-8">
                                               <input placeholder="Registered from" class="form-control datepicker" type="text" name="created_from"
                                                    value="{{ request()->created_from }}" />
                                                <input placeholder="Registered till" class="form-control datepicker"  type="text" name="created_to"
                                                       value="{{ request()->created_to }}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 text-right">
                                        <button type="submit" class="btn btn-primary">Search</button>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>
                    <table class="table table-hover table-striped table-bordered table-condensed responsive">
                        <thead>
                        <tr>
                            @foreach (['user', 'email'] as $column)
                            <th>
                                <a href="{{ route('admins.index', $request->merge([
                                    'sort' => request()->order == $column && request()->sort == 'ASC' ? 'DESC' : 'ASC',
                                    'order' => $column
                                ])->all()) }}">
                                    @lang('general.' . $column)
                                </a>
                            </th>
                            @endforeach
                            <th>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($users as $user)
                            <tr>
                                <td data-label="@lang('general.user')">{{ $user->name }}</td>
                                <td data-label="@lang('general.email')">{{ $user->email }}</td>
                                <td class="td-action">
                                    <a href="{{ route('admins.edit', $user) }}" class="float-sm-left d-inline-block d-sm-inline btn btn-success btn-sm pull-left">
                                        <i class="fas fa-pencil-alt"></i>
                                    </a>
                                    @if($user->id != 1)
                                    <form class="float-sm-left ml-2 d-inline-block d-sm-inline" onsubmit="if(confirm('@lang('general.deleting')')){ return true }else{ return false }" action="{{ route('admins.destroy', $user) }}" method="post">
                                        <input type="hidden" name="_method" value="delete" />
                                        {{csrf_field()}}
                                        <button type="submit" class="btn-sm remove_item btn btn-danger pull-left"><i class="fa fa-trash"></i></button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5" class="text-center">
                                    <b>@lang('general.empty_data')</b>
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <div class="col-sm-12 text-center">
                        <nav aria-label="page navigation" class="justify-content-center mt-3">
                            {{ $users->appends(request()->except('page'))->links() }}
                        </nav>
                    </div>

                </div>
            </div>

        </section>
    </div>
@endsection