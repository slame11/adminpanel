@extends('layouts.master')


@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>@lang('general.user_creating')</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('general.dashboard')</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admins.index') }}">@lang('general.admins')</a></li>
                            <li class="breadcrumb-item active">@lang('general.create')</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            {!! Form::model($user, [
                'route' => ['admins.store'],
                'method' => 'post',
                'files' => true,
                'id' => 'user_create_form'
            ]) !!}
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">@lang('general.users')</h3>
                    <div class="card-tools">
                        {!! Form::submit(__('general.save'), ['class' => 'btn btn-success', 'form' => 'user_create_form']) !!}
                    </div>
                </div>
                <div class="card-body">
                    @include('admin.partials.form')
                    {!! Form::hidden('created_by', Auth::id()) !!}
                </div>
            </div>

        </section>
    </div>
    <div class="row">
        <div class="col-md-12">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    <div class="pull-right">
                        {!! Form::submit(__('general.save'), ['class' => 'btn btn-success']) !!}

                        <a href="{{ route('users.index') }}" class="btn btn-default"><i class="fa fa-share"></i></a>
                    </div>
                </div>
                <div class="box-body">

                </div>
            </div>

        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(function () {

            function randString(id){
                var dataSet = $(id).attr('data-character-set').split(',');
                var possible = '';
                if($.inArray('a-z', dataSet) >= 0){
                    possible += 'abcdefghijklmnopqrstuvwxyz';
                }
                if($.inArray('A-Z', dataSet) >= 0){
                    possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                }
                if($.inArray('0-9', dataSet) >= 0){
                    possible += '0123456789';
                }
                if($.inArray('#', dataSet) >= 0){
                    possible += '![]{}()%&*$#^<>~@|';
                }
                var text = '';
                for(var i=0; i < $(id).attr('data-size'); i++) {
                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                return text;
            }

            // Create a new password
            $(".getNewPass").click(function(){
                var field = $(this).closest('div').find('input[rel="gp"]');
                var password = randString(field);
                field.val(password).prop("type", "text");
                $('input[rel="gp_confirm"]').val(password).prop("type", "text");
            });

            // Auto Select Pass On Focus
            $('input[rel="gp"]').on("click", function () {
                $(this).select();
            });


        });
    </script>
@endsection
