<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12 ">
            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', __('general.username')) !!}
                {!! Form::text('name', $user->username, [
                    'class' => 'form-control',
                    'placeholder' => __('general.username'),
                    'size' => 40,
                    'required'=>'true'
                ]) !!}
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('email', __('general.email')) !!}
                {!! Form::text('email',$user->email, [
                    'class' => 'form-control',
                    'placeholder' => __('general.email'),
                    'size' => 70,
                    'required'=>'true'
                ]) !!}
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group">
                <label for="">@lang('general.password')</label>
                <div class="input-group {{ $errors->has('password') ? 'has-error' : ''}}">
                    <input type="password" class="form-control" name="password" placeholder="@lang('general.password')" size="255"
                        data-character-set='a-z,A-Z,0-9' rel='gp' data-size='16' {{ !$user->id ? 'required' : '' }} />
                    <span class="input-group-btn" id="hashpassword">
                        <button type="button" class="btn btn-default getNewPass">
                            <i class="fas fa-sync"></i>
                        </button>
                    </span>
                </div>
                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group">
                <label for="">@lang('general.password_confirm')</label>
                <input type="password" class="form-control" name="password_confirmation" placeholder="@lang('general.repeat password')" size="255" rel="gp_confirm" />
            </div>

        </div>

    </div>

    <div class="col-sm-6"></div>
</div>

{{--@include('admin.user.partials.translations')--}}