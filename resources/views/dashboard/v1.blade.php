@extends('layouts.master') 
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Dashboard</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v1</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content" id="statistic_content">
    <div class="container-fluid">
      <div class="card">
        <div class="card-header p-0">
          <h3 class="card-title p-3">
            Statistic
          </h3>
        </div>
        <div class="card-body px-0">

          <div class="row">
            <div class="col-sm-4">
              <div class="form-group row my-0">
                <label for="hour" class="col-sm-3 text-right col-4 text-sm" >Any</label>
                <div class="col-sm-9 col-8"><input id="hour" checked type="radio" name="time" value="any" /> </div>
              </div>
              <div class="form-group row my-0">
                <label for="hour" class="col-sm-3 text-right col-4 text-sm" >1 hour</label>
                <div class="col-sm-9 col-8"><input id="hour" type="radio" name="time" value="hour" /> </div>
              </div>
              <div class="form-group row my-0">
                <label for="day"  class="col-sm-3 text-right col-4 text-sm" >Day</label>
                <div class="col-sm-9 col-8"><input id="day" type="radio" name="time" value="day" /> </div>
              </div>
              <div class="form-group row my-0">
                <label for="week" class="col-sm-3 text-right col-4 text-sm" >Week</label>
                <div class="col-sm-9 col-8"><input id="week" type="radio" name="time" value="week" /> </div>
              </div>
              <div class="form-group row my-0">
                <label for="month" class="col-sm-3 text-right col-4 text-sm" >Month</label>
                <div class="col-sm-9 col-8"><input id="month" type="radio" name="time" value="month" /> </div>
              </div>
              <div class="form-group row my-0">
                <label for="custom"  class="col-sm-3 text-right col-4 text-sm" >Custom</label>
                <div class="col-sm-9 col-8"><input id="custom" type="radio" name="time" value="custom" /> </div>
              </div>
              <div class="custom_time d-none">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input class="datepicker form-control" placeholder="Time from" value="" name="time_from" type="text" />
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input class="datepicker form-control" placeholder="Time to" value="" name="time_to" type="text" />
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group  row my-0" >
                <label for="custom"  class="col-sm-3 col-4  text-right text-sm" >Unsdg types</label>
                <div class="col-sm-9">
                  <select name="undsg_type_select" class="form-control select2 select2_multiply" multiple>
                    <option selected>Any</option>
                    @foreach($unsdg_types as $unsdg_type)
                      <option value="{{$unsdg_type->id}}">{{$unsdg_type->title}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group  row my-0" >
                <label for="custom"  class="col-sm-3 col-4 text-right text-sm" >Tag types</label>
                <div class="col-sm-9">
                  <select name="tags_type_select" class="form-control select2 select2_multiply" multiple>
                    <option selected>Any</option>
                    @foreach($tag_types as $tag_type)
                      <option value="{{$tag_type->id}}">{{$tag_type->title}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
                <div class="text-center mt-5 mb-5 mb-sm-0">
                    <button id="filter_btn" class="btn btn-primary">Filter</button>
                </div>
            </div>
            <div class="col-sm-8 dashboard_charts mb-5">
              <div class="row m-0">
                <div class="col-sm-6">
                  <div class="small-box bg-primary">
                    <div class="inner">
                      <h3 id="all_users_count">{{$all_users}}</h3>

                      <p>Registered users</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-users"></i>
                    </div>
                    <a href="{{route('users.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="small-box bg-success">
                    <div class="inner">
                      <h3 id="online_users_count">{{$all_users_online}}</h3>

                      <p>Online users</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-user-circle"></i>
                    </div>
                  </div>
                </div>
              </div>
              <h3 class="text-center">Users Graphs</h3>
              <div class="chartjs-wrapper">
                <canvas id="users-online-chart" height="200"></canvas>
              </div>
              <div class="chartjs-wrapper">
                <canvas id="users-registered-chart" height="200"></canvas>
              </div>
            </div>
            <div class="col-sm-12">
              <h3 class="text-center">Posts Graphs</h3>
              <div class="row mx-0">
                <div class="col-sm-3">
                  <div class="small-box bg-primary">
                    <div class="inner">
                      <h3 id="all_posts">{{$all_posts}}</h3>

                      <p>Posts</p>
                    </div>
                    <div class="icon">
                      <i class="far fa-clipboard"></i>
                    </div>
                    <a href="{{route('statuses.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="small-box bg-warning">
                    <div class="inner">
                      <h3 id="all_posts_unsdg">{{$all_posts_unsdg}}</h3>

                      <p>Posts with UNSDG Tag</p>
                    </div>
                    <div class="icon">
                      <i class="far fa-clipboard"></i>
                    </div>
                    <a href="{{route('statuses.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="small-box bg-danger">
                    <div class="inner">
                      <h3 id="all_posts_with_tag">{{$all_posts_with_tag}}</h3>

                      <p>Posts with #Tag</p>
                    </div>
                    <div class="icon">
                      <i class="far fa-clipboard"></i>
                    </div>
                    <a href="{{route('statuses.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="small-box bg-success">
                    <div class="inner">
                      <h3 id="all_posts_with_snippet">{{$all_posts_with_snippet}}</h3>

                      <p>Posts with Snippets</p>
                    </div>
                    <div class="icon">
                      <i class="far fa-clipboard"></i>
                    </div>
                    <a href="{{route('statuses.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="chartjs-wrapper">
                    <canvas id="unsdg-chart" height="200"></canvas>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="chartjs-wrapper mt-3">
                    <canvas id="tags-chart" height="200"></canvas>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="chartjs-wrapper mt-3">
                    <canvas id="genderunsdg-chart" height="200"></canvas>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="chartjs-wrapper mt-3">
                    <canvas id="gendertags-chart" height="200"></canvas>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="chartjs-wrapper mt-3">
                    <canvas id="ageunsdg-chart" height="200"></canvas>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="chartjs-wrapper mt-3">
                    <canvas id="agetags-chart" height="200"></canvas>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="chartjs-wrapper mt-3">
                    <canvas id="industry_types_to_posts" height="200"></canvas>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="chartjs-wrapper mt-3">
                    <canvas id="industry_types_to_users" height="200"></canvas>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="chartjs-wrapper mt-3">
                    <canvas id="subindustry_types_to_posts" height="200"></canvas>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="chartjs-wrapper mt-3">
                    <canvas id="subindustry_types_to_users" height="200"></canvas>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-12 tables_container mt-5">
              <h3 class="text-center">Posts Tables</h3>
              <div class="row">
                <div class="col-sm-6">
                  <div class="card ">
                    <div class="card-header">
                      Top 10 Companies using SDG Tag
                    </div>
                    <div class="card-body p-2">
                      <table id="top_10_companies_undsg_tags" class="datatable table table-bordered w-100">
                        <thead>
                        <tr>
                          <th>
                            Company name
                          </th>
                          <th>
                           Volume of SDG Tag in posts
                          </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($top_10_companies_undsg_tags as $top_item)
                          <tr>
                            <td>{{$top_item->title}}</td>
                            <td>{{$top_item->count}}</td>
                          </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="card ">
                    <div class="card-header">
                      Top 10 Companies using #Tag
                    </div>
                    <div class="card-body p-2">
                      <table id="top_10_companies_tags" class="datatable table table-bordered w-100">
                        <thead>
                        <tr>
                          <th>
                            Company name
                          </th>
                          <th>
                            Volume of  #Tag in posts
                          </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($top_10_companies_tags as $top_item)
                          <tr>
                            <td>{{$top_item->title}}</td>
                            <td>{{$top_item->count}}</td>
                          </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="card ">
                    <div class="card-header">
                      Top 10 NonProfits using SDG Tag
                    </div>
                    <div class="card-body p-2">
                      <table id="top_10_nonpofits_undsg_tags" class="datatable table table-bordered w-100">
                        <thead>
                        <tr>
                          <th>
                            Nonprofit name
                          </th>
                          <th>
                            Volume of  SDG Tag in posts
                          </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($top_10_nonpofits_undsg_tags as $top_item)
                          <tr>
                            <td>{{$top_item->title}}</td>
                            <td>{{$top_item->count}}</td>
                          </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="card ">
                    <div class="card-header">
                      Top 10 NonProfits using #Tag
                    </div>
                    <div class="card-body p-2">
                      <table id="top_10_nonpofits_tags" class="datatable table table-bordered w-100">
                        <thead>
                        <tr>
                          <th>
                            Nonprofit name
                          </th>
                          <th>
                            Volume of  #Tag in posts
                          </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($top_10_nonpofits_tags as $top_item)
                          <tr>
                            <td>{{$top_item->title}}</td>
                            <td>{{$top_item->count}}</td>
                          </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="card ">
                    <div class="card-header">
                      Top 10 SDGs Trending 
                    </div>
                    <div class="card-body p-2">
                      <table id="top_10_unsdg_tags" class="datatable table table-bordered w-100">
                        <thead>
                        <tr>
                          <th>
                            SDG Tag
                          </th>
                          <th>
                            Volume in posts
                          </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($top_10_unsdg_tags as $top_item)
                          <tr>
                            <td>{{$top_item->title}}</td>
                            <td>{{$top_item->count}}</td>
                          </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="card ">
                    <div class="card-header">
                      Top 10 #TAGs Trending
                    </div>
                    <div class="card-body p-2">
                      <table id="top_10_tags" class="datatable table table-bordered w-100">
                        <thead>
                        <tr>
                          <th>
                            #Tag
                          </th>
                          <th>
                            Volume in posts
                          </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($top_10_tags as $top_item)
                          <tr>
                            <td>{{$top_item->title}}</td>
                            <td>{{$top_item->count}}</td>
                          </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="card ">
                    <div class="card-header">
                      Top 10 Snippets Trending
                    </div>
                    <div class="card-body p-2">
                      <table id="top_10_snippets" class="datatable table table-bordered w-100">
                        <thead>
                        <tr>
                          <th>
                            Snippet tag
                          </th>
                          <th>
                            Volume in posts
                          </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($top_10_snippets as $top_item)
                          <tr>
                            <td>{{$top_item->title}}</td>
                            <td>{{$top_item->count}}</td>
                          </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- /.row (main row) -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div id="elementH"></div>
@endsection

@section('javascript')
<!-- jQuery -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<!-- Bootstrap 4 -->
<script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="/dist/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="/dist/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="/dist/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/dist/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="/dist/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="/dist/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="/dist/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="/dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="/dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/dist/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/dist/js/demo.js"></script>
<script src="/dist/plugins/select2/select2.min.js"></script>
<!-- OPTIONAL SCRIPTS -->
<script src="/dist/plugins/chart.js/Chart.min.js"></script>

<script>
    $.widget.bridge('uibutton', $.ui.button)

    $(function () {

        var data = {
            undsg_tags : {data : [{{implode(',',$organisation_types_to_unsdg_tags)}}], labels: [@foreach($organisation_types as $organisation_type) '{{$organisation_type->title}}', @endforeach 'Individual']},
            tags :  {data : [{{implode(',',$organisation_types_to_tags)}}], labels: [@foreach($organisation_types as $organisation_type) '{{$organisation_type->title}}', @endforeach 'Individual']},
            gender_to_unsdg_tags :  {data : [{{implode(',',$gender_to_unsdg_tags)}}], labels: [@foreach($gender_types as $gender_type) '{{$gender_type->title}}', @endforeach]},
            gender_to_tags : {data : [{{implode(',',$gender_to_tags)}}], labels: [@foreach($gender_types as $gender_type) '{{$gender_type->title}}', @endforeach]},
            age_to_undsg_tags : {data : [{{implode(',',$age_to_undsg_tags)}}], labels: [@foreach($ages_variants as $ages_variant) '{{$ages_variant[0]}} - {{$ages_variant[1]}}', @endforeach]},
            age_to_tags:  {data : [{{implode(',',$age_to_tags)}}], labels: [@foreach($ages_variants as $ages_variant) '{{$ages_variant[0]}} - {{$ages_variant[1]}}', @endforeach]},
            user_types : {data : [{{implode(',',$user_registered_by_type)}}], labels: [@foreach($organisation_types as $organisation_type) '{{$organisation_type->title}}', @endforeach 'Individual']},
            online_types : {data : [{{implode(',',$user_logged_by_type)}}], labels: [@foreach($organisation_types as $organisation_type) '{{$organisation_type->title}}', @endforeach 'Individual']},
            industry_types_to_posts : {data : [{{implode(',',$industry_types_to_posts['values'])}}],labels : [{!! implode(',',$industry_types_to_posts['labels']) !!}]},
            industry_types_to_users : {data : [{{implode(',',$industry_types_to_users['values'])}}],labels : [{!! implode(',',$industry_types_to_users['labels']) !!}]},
            subindustry_types_to_posts : {data : [{{implode(',',$subindustry_types_to_posts['values'])}}],labels : [{!! implode(',',$subindustry_types_to_posts['labels']) !!}]},
            subindustry_types_to_users : {data : [{{implode(',',$subindustry_types_to_users['values'])}}],labels : [{!! implode(',',$subindustry_types_to_users['labels']) !!}]}
        };
        setCharts(data);

    })
</script>
@stop