@extends('layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>@lang('general.edit_post')</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('general.dashboard')</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('statuses.index') }}">@lang('general.posts')</a></li>
                            <li class="breadcrumb-item active">@lang('general.edit')</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            {!! Form::model($status, ['route' => [
                'statuses.update', $status],
                'method' => 'put',
                'files' => true,
                'id' => 'status_create_form'
            ]) !!}
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">@lang('general.posts')</h3>
                    <div class="card-tools">
                        {!! Form::submit(__('general.save'), ['class' => 'btn btn-success', 'form' => 'status_create_form']) !!}
                    </div>
                </div>
                <div class="card-body">
                    @include('posts.partials.form')
                    {!! Form::hidden('modified_by', Auth::id()) !!}
                </div>
            </div>

        </section>
    </div>
@endsection
