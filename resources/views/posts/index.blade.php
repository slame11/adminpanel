@extends('layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>@lang('general.posts')</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">@lang('general.home')</a></li>
                            <li class="breadcrumb-item active">@lang('general.posts')</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">@lang('general.posts')</h3>

                    <div class="card-tools">
                        <a href="{{ route('statuses.create') }}" class="btn btn-sm btn-success add_new_user">
                            @lang('general.new')
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <form class="" action="" method="get">


                        <div class="card mt-3">
                            <div class="card-header">
                                <h3 class="card-title">Filter</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-4 text-right" >User</label>
                                            <div class="col-sm-8">
                                                <select name="user" class="form-control">
                                                    <option value="">any</option>
                                                    @foreach ($users  as $user)
                                                        <option {{ request()->user == $user->id ? 'selected' : '' }}  value="{{ $user->id }}">
                                                            {{ $user->username }} ({{ $user->firstname }} {{ $user->lastname }})
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-4 text-right" >Organisation</label>
                                            <div class="col-sm-8">
                                                <select name="organisation" class="form-control">
                                                    <option value="">any</option>
                                                    @foreach ($organisations  as $organisation)
                                                        <option {{ request()->organisation == $organisation->id ? 'selected' : '' }}  value="{{ $organisation->id }}">
                                                            {{ $organisation->title }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-4 text-right" >Post created</label>
                                            <div class="col-sm-8">
                                                <input placeholder="Registered from" class="form-control datepicker" type="text" name="created_from"
                                                       value="{{ request()->created_from }}" />
                                                <input placeholder="Registered till" class="form-control datepicker"  type="text" name="created_to"
                                                       value="{{ request()->created_to }}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 text-right">
                                        <button type="submit" class="btn btn-primary">Search</button>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>
                    <table class="table table-hover table-striped table-bordered table-condensed responsive">
                        <thead>
                        <tr>
                            @foreach (['statuses.created_at', 'users.username', 'organizations.title', 'text'] as $column)
                            <th>
                                <a href="{{ route('statuses.index', $request->merge([
                                    'sort' => request()->order == $column && request()->sort == 'ASC' ? 'DESC' : 'ASC',
                                    'order' => $column
                                ])->all()) }}">
                                    @lang('general.' . $column)
                                </a>
                            </th>
                            @endforeach
                            <th>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($posts as $post)

                            <tr>
                                <td  data-label="@lang('general.statuses.created_at')">{{$post->created_at}}</td>
                                <td  data-label="@lang('general.users.username')">@if(isset($post->user))<a href="{{route('users.edit',['id' =>  $post->user->id])}}">{{ isset($post->user->username) ? $post->user->username : '' }} ({{ isset($post->user->firstname) ? $post->user->firstname : '' }} {{ isset($post->user->lastname) ? $post->user->lastname : '' }} )</a>   @endif</td>
                                <td  data-label="@lang('general.organizations.title')">@if(isset($post->organization)) <a href="#">{{$post->organization->title}}</a>@endif </td>
                                <td  data-label="@lang('general.text')">{{ $post->body }}</td>
                                <td class="td-action">
                                    <a href="{{ route('statuses.edit', $post) }}" class="float-sm-left d-inline-block d-sm-inline btn btn-success btn-sm pull-left">
                                        <i class="fas fa-pencil-alt"></i>
                                    </a>

                                    <form class="float-sm-left ml-2 d-inline-block d-sm-inline" onsubmit="if(confirm('@lang('general.deleting')')){ return true }else{ return false }" action="{{ route('statuses.destroy', $post) }}" method="post">
                                        <input type="hidden" name="_method" value="delete" />
                                        {{csrf_field()}}
                                        <button type="submit" class="btn-sm remove_item btn btn-danger pull-left"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5" class="text-center">
                                    <b>@lang('general.empty_data')</b>
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <div class="col-sm-12 text-center">
                        <nav aria-label="page navigation" class="justify-content-center mt-3">
                            {{ $posts->appends(request()->except('page'))->links() }}
                        </nav>
                    </div>

                </div>
            </div>

        </section>
    </div>
@endsection