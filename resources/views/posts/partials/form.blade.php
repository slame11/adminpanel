<div class="row">
    <div class="col-6">
        <div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
            {!! Form::label('user_id', __('general.user')) !!}
            {!! Form::select('user_id', $users, $status->user_id, [
                'class' => 'form-control',
                'placeholder' => __('general.user'),
            ]) !!}
            {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col-6">
        <div class="form-group {{ $errors->has('organization_id') ? 'has-error' : ''}}">
            {!! Form::label('organization_id', __('general.organization')) !!}
            {!! Form::select('organization_id', $organizations, $status->organization_id, [
                'class' => 'form-control',
                'placeholder' => __('general.organization'),
            ]) !!}
            {!! $errors->first('organization_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col-6">
        <div class="form-group {{ $errors->has('view_type_id') ? 'has-error' : ''}}">
            {!! Form::label('view_type_id', __('general.view_type')) !!}
            {!! Form::select('view_type_id', $view_types, $status->view_type_id, [
                'class' => 'form-control',
                'placeholder' => __('general.view_type'),
            ]) !!}
            {!! $errors->first('view_type_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col-6">
        <div class="form-group {{ $errors->has('parent_id') ? 'has-error' : ''}}">
            {!! Form::label('parent_id', __('general.parent')) !!}
            {!! Form::select('parent_id', $parents, $status->parent_id, [
                'class' => 'form-control',
                'placeholder' => __('general.parent'),
            ]) !!}
            {!! $errors->first('parent_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="col-12">
        <div class="form-group {{ $errors->has('body') ? 'has-error' : ''}}">
            {!! Form::label('body', __('general.body')) !!}
            {!! Form::textarea('body', $status->body,[
                'class' => 'form-control',
                'placeholder' => __('general.body'),
            ]) !!}
            {!! $errors->first('body', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="col-6">
        <div class="form-group {{ $errors->has('snippet_text') ? 'has-error' : ''}}">
            {!! Form::label('job', __('general.snippet_text')) !!}
            {!! Form::text('job', $status->snippet_text,[
                'class' => 'form-control',
                'placeholder' => __('general.snippet_text'),
                'size' => 255,
            ]) !!}
            {!! $errors->first('snippet_text', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col-6">
        <div class="form-group {{ $errors->has('snippet_number') ? 'has-error' : ''}}">
            {!! Form::label('snippet_number', __('general.snippet_number')) !!}
            {!! Form::text('snippet_number', $status->snippet_number,[
                'class' => 'form-control',
                'placeholder' => __('general.snippet_number'),
                'size' => 255,
            ]) !!}
            {!! $errors->first('snippet_number', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>