@extends('layouts.master')


@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>@lang('general.post_creating')</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i>@lang('general.dashboard')</a></li>
                            <li><a href="{{ route('statuses.index') }}"><i class="fa fa-thumbs-up"></i>@lang('general.posts')</a></li>
                            <li class="active"><i class="fa fa-thumbs-up"></i>@lang('general.edit')</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            {!! Form::model($status, [
                'route' => ['statuses.store'],
                'method' => 'post',
                'files' => true,
                'id' => 'status_create_form'
            ]) !!}
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">@lang('general.posts')</h3>
                    <div class="card-tools">
                        {!! Form::submit(__('general.save'), ['class' => 'btn btn-success', 'form' => 'status_create_form']) !!}
                    </div>
                </div>
                <div class="card-body">
                    @include('posts.partials.form')
                    {!! Form::hidden('created_by', Auth::id()) !!}
                </div>
            </div>

        </section>
    </div>
</div>
@endsection
