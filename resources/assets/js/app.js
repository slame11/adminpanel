
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('jquery/dist/jquery.min.js');
require('./bootstrap');
require('bootstrap-datepicker/dist/js/bootstrap-datepicker.js');
require('datatables.net-bs4/js/dataTables.bootstrap4.js');

