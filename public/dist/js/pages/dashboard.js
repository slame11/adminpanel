/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/
var charts_data = {
    unsdg_chart : {
        selector : '#unsdg-chart',
        chart : undefined,
        key : 'undsg_tags',
        chart_title : 'User Types who used the UNSDG Tags'
    },
    tags : {
        selector : '#tags-chart',
        chart : undefined,
        key : 'tags',
        chart_title : 'User Types/Volume who used  #Tags'
    },
    genderunsdg : {
        selector : '#genderunsdg-chart',
        chart : undefined,
        key : 'gender_to_unsdg_tags',
        chart_title : 'Gender Identities who used UNSDG Tags'
    },
    gendertags: {
        selector : '#gendertags-chart',
        chart : undefined,
        key : 'gender_to_tags',
        chart_title : 'Gender Identities who used #Tags'
    },
    ageunsdg : {
        selector: '#ageunsdg-chart',
        chart: undefined,
        key : 'age_to_undsg_tags',
        chart_title : 'Age Groups (Individuals) who used the UNSDG Tags'
    },
    agetags : {
        selector: '#agetags-chart',
        chart: undefined,
        key : 'age_to_tags',
        chart_title : 'Age Groups (Individuals) who used the #Tags'
    },
    user_types : {
        selector: '#users-registered-chart',
        chart: undefined,
        key : 'user_types',
        chart_title : 'Registered Users Volume by Type and Aggregate/Time (Average)'
    },
    online_types : {
        selector: '#users-online-chart',
        chart: undefined,
        key : 'online_types',
        chart_title : 'Online Users Volume by Type and Aggregate/Time (Average)'
    },
    industry_types_to_posts : {
        selector: '#industry_types_to_posts',
        chart: undefined,
        key: 'industry_types_to_posts',
        chart_title : 'Posts by industry segment'
    },
    industry_types_to_users: {
        selector: '#industry_types_to_users',
        chart: undefined,
        key: 'industry_types_to_users',
        chart_title : 'Users by industry segment'
    },
    subindustry_types_to_posts: {
        selector: '#subindustry_types_to_posts',
        chart: undefined,
        key: 'subindustry_types_to_posts',
        chart_title : 'Posts by subindustry segment'
    },
     subindustry_types_to_users :{
         selector: '#subindustry_types_to_users',
         chart: undefined,
         key: 'subindustry_types_to_users',
         chart_title : 'Users by subindustry segment'
     }
    }
;
var tags, unsdg, genderunsdg, gendertags;
function setCharts(data){
    //undg tags chart

    var mode      = 'index';
    var intersect = true;

    var ticksStyle = {
        fontColor: '#495057',
        fontStyle: 'bold',
    }

    $.each(charts_data, function (key, val) {
        var dom_element = $(val.selector);
        if(typeof val.chart != 'undefined'){
            val.chart.destroy();
        }
        val.chart  = new Chart(dom_element, {
            type   : 'bar',

            data   : {
                labels  : data[val.key].labels,
                datasets: [
                    {
                        backgroundColor: '#007bff',
                        borderColor    : '#007bff',
                        data           : data[val.key].data
                    },

                ]
            },

            options: {
                animation: {
                    duration: 1,
                    onComplete: function () {
                        var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';

                        this.data.datasets.forEach(function (dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function (bar, index) {
                                //calculate the total of this data set
                                var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                                    return previousValue + currentValue;
                                });
                                var currentValue = dataset.data[index];
                                var percentage = Math.floor(((currentValue/total) * 100)+0.5);
                                ctx.fillText(currentValue + (percentage >0 ?   ' (' + percentage + "%)" : ''), bar._model.x, bar._model.y - 5);
                            });
                        });
                    }
                },
                title: {
                    display: true,
                    text: val.chart_title
                },
                maintainAspectRatio: false,
                tooltips           : {
                    mode     : mode,
                    intersect: intersect,
                        callbacks: {
                            label: function(tooltipItem, data) {

                                var dataset = data.datasets[tooltipItem.datasetIndex];

                                //calculate the total of this data set
                                var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                                    return previousValue + currentValue;
                                });
                                var currentValue = dataset.data[tooltipItem.index];
                                var percentage = Math.floor(((currentValue/total) * 100)+0.5);

                                return currentValue + ' (' + percentage + "%)";
                            }
                        }

                },
                hover              : {
                    mode     : mode,
                    intersect: intersect
                },
                legend             : {
                    display: false
                },
                scales             : {
                    yAxes: [{
                        scaleLabel  : {labelString:'Volume',display: true},
                        gridLines: {
                            display      : true,
                            lineWidth    : '4px',
                            color        : 'rgba(0, 0, 0, .2)',
                            zeroLineColor: 'transparent'
                        },
                        ticks    : $.extend({
                            beginAtZero: true,


                        }, ticksStyle)
                    }],
                    xAxes: [{
                        display  : true,
                        gridLines: {
                            display: false
                        },
                        ticks    : ticksStyle
                    }]
                }
            }
        });
    });

}
$(function () {
    'use strict'

    $('.select2_multiply').select2();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('.datatable').dataTable({"order": [[ 1, "desc" ]]});
    $('input[name=time]').on('change',function () {
        if($(this).attr('id') === 'custom'){
          $('.custom_time').removeClass('d-none')
        }else{
            $('.custom_time').addClass('d-none')
        }
    });



    $('#filter_btn').on('click',function () {
        var undsg_tags = $('select[name=undsg_type_select]').val();
        var tags = $('select[name=tags_type_select]').val();
        var time_period = $('input[name=time]:checked').val();
        var time_from = $('input[name=time_from]').val();
        var time_to = $('input[name=time_to]').val();
        $.ajax({
            type:'POST',
            url:'/dashboard/dashboard_charts',
            data:{undsg_tags : undsg_tags, tags: tags, time_period: time_period, time_from: time_from,time_to : time_to},
            success:function(data) {
                setCharts(data);
                var tables = ['top_10_companies_undsg_tags',
                    'top_10_companies_tags',
                    'top_10_nonpofits_undsg_tags',
                    'top_10_nonpofits_tags',
                    'top_10_unsdg_tags',
                    'top_10_tags',
                    'top_10_snippets'
                ];
                $.each(tables, function (key, val) {
                    var table =  $('#' + val);
                    table.dataTable().fnDestroy();
                    table.find('tbody').empty();
                    $.each(data[val],function (key, val) {
                        table.find('tbody').append('<tr><td>'+val.title+'</td><td>'+val.count+'</td></tr>');
                    })
                    table.dataTable({"order": [[ 1, "desc" ]]});
                });

                $('#all_users_count').text(data.all_users);
                $('#online_users_count').text(data.all_users_online);
                $('#all_posts').text(data.all_posts);
                $('#all_posts_unsdg').text(data.all_posts_unsdg);
                $('#all_posts_with_tag').text(data.all_posts_with_tag);
                $('#all_posts_with_snippet').text(data.all_posts_with_snippet);


            }
        });
    })
})
